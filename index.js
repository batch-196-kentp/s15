
/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

let firstName = "Kent Aloen";
console.log("First Name: " + firstName);

let lastName = "Pedros";
console.log("Last Name: " + lastName);

let age = 27
console.log("Age: " + age);

let hobbies1 = "Hobbies:"
console.log(hobbies1);
let hobbies2 = ["basketball", "gaming", "reading", "sleeping"]
console.log(hobbies2);

let workAddress = "Work Address:";
console.log(workAddress);
let address = {
	houseNumber: 7685,
	street: "Cajeta St",
	city: "Ozamis City",
	state: "Mis Occ"
};
console.log(address);

/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"]
	console.log("My Friends are:")
	console.log(friends);

	let profile = {
		userName: "captain america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: true
	}
	console.log("My Full Profile: ")
	console.log(profile);

	let bestFriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestFriend);

	// const lastLocation = "Arctic Ocean";
	let lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);
